<?php

/**
 * /entity_name/ view callback.
 */
function /module_name/_view($/entity_name/) {
  drupal_set_title(entity_label('/entity_name/', $/entity_name/));
  return entity_view('/entity_name/', array(entity_id('/entity_name/', $/entity_name/) => $/entity_name/), 'full');
}
