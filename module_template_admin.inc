<?php
/**
 * Generates the /entity_name/ type editing form.
 */
function /entity_name/_type_form($form, &$form_state, $/entity_name/_type, $op = 'edit') {

  if ($op == 'clone') {
    $/entity_name/_type->label .= ' (cloned)';
    $/entity_name/_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $/entity_name/_type->label,
    '#description' => t('The human-readable name of this /entity_name/ type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($/entity_name/_type->type) ? $/entity_name/_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $/entity_name/_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => '/entity_name/_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this /entity_name/ type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($/entity_name/_type->description) ? $/entity_name/_type->description : '',
    '#description' => t('Description about the /entity_name/ type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save /entity_name/ type'),
    '#weight' => 40,
  );

  if (!$/entity_name/_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete /entity_name/ type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('/entity_name/_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing /entity_name/_type.
 */
function /entity_name/_type_form_submit(&$form, &$form_state) {
  $/entity_name/_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  /entity_name/_type_save($/entity_name/_type);

  // Redirect user back to list of /entity_name/ types.
  $form_state['redirect'] = 'admin/structure//entity_name/-types';
}

function /entity_name/_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure//entity_name/-types/' . $form_state['/entity_name/_type']->type . '/delete';
}

/**
 * /entity_name/ type delete form.
 */
function /entity_name/_type_form_delete_confirm($form, &$form_state, $/entity_name/_type) {
  $form_state['/entity_name/_type'] = $/entity_name/_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['/entity_name/_type_id'] = array('#type' => 'value', '#value' => entity_id('/entity_name/_type' ,$/entity_name/_type));
  return confirm_form($form,
    t('Are you sure you want to delete /entity_name/ type %title?', array('%title' => entity_label('/entity_name/_type', $/entity_name/_type))),
    '/entity_name//' . entity_id('/entity_name/_type' ,$/entity_name/_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * /entity_name/ type delete form submit handler.
 */
function /entity_name/_type_form_delete_confirm_submit($form, &$form_state) {
  $/entity_name/_type = $form_state['/entity_name/_type'];
  /entity_name/_type_delete($/entity_name/_type);

  watchdog('/entity_name/_type', '@type: deleted %title.', array('@type' => $/entity_name/_type->type, '%title' => $/entity_name/_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $/entity_name/_type->type, '%title' => $/entity_name/_type->label)));

  $form_state['redirect'] = 'admin/structure//entity_name/-types';
}

/**
 * Page to select /entity_name/ Type to add new /entity_name/.
 */
function /entity_name/_admin_add_page() {
  $items = array();
  foreach (/entity_name/_types() as $/entity_name/_type_key => $/entity_name/_type) {
    $items[] = l(entity_label('/entity_name/_type', $/entity_name/_type), '/entity_name//add/' . $/entity_name/_type_key);
  }
  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of /entity_name/ to create.')));
}

/**
 * Add new /entity_name/ page callback.
 */
function /entity_name/_add($type) {
  $/entity_name/_type = /entity_name/_types($type);

  $/entity_name/ = entity_create('/entity_name/', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('/entity_name/_type', $/entity_name/_type))));

  $output = drupal_get_form('/entity_name/_form', $/entity_name/);

  return $output;
}

/**
 * /entity_name/ Form.
 */
function /entity_name/_form($form, &$form_state, $/entity_name/) {
  $form_state['/entity_name/'] = $/entity_name/;
  $/entity_name/_id = entity_id('/entity_name/', $/entity_name/);

  $form['/entity_name/'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('/entity_name/'),
    '#default_value' => $/entity_name/->/entity_name/,
    //'#suffix' => '',
  );
 
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $/entity_name/->description,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $/entity_name/->uid,
  );

  field_attach_form('/entity_name/', $/entity_name/, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save /entity_name/'),
    '#submit' => $submit + array('/entity_name/_form_submit'),
  );

  // Show Delete button if we edit /entity_name/.

  if (!empty($/entity_name/_id) && /entity_name/_access('edit', $/entity_name/)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('/entity_name/_form_submit_delete'),
    );
  }

  $form['#validate'][] = '/entity_name/_form_validate';

  return $form;
}

function /entity_name/_form_validate($form, &$form_state) {

}

/**
 * /entity_name/ submit handler.
 */
function /entity_name/_form_submit($form, &$form_state) {

  $/entity_name/ = $form_state['/entity_name/'];
  entity_form_submit_build_entity('/entity_name/', $/entity_name/, $form, $form_state);

  /entity_name/_save($/entity_name/);

  $/entity_name/_uri = entity_uri('/entity_name/', $/entity_name/);

  $form_state['redirect'] = $/entity_name/_uri['path'];

  drupal_set_message(t('/entity_name/ %title saved.', array('%title' => entity_label('/entity_name/', $/entity_name/))));


}

function /entity_name/_form_submit_delete($form, &$form_state) {
  $/entity_name/ = $form_state['/entity_name/'];
  $/entity_name/_uri = entity_uri('/entity_name/', $/entity_name/);
  $form_state['redirect'] = $/entity_name/_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function /entity_name/_delete_form($form, &$form_state, $/entity_name/) {
  $form_state['/entity_name/'] = $/entity_name/;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['/entity_name/_type_id'] = array('#type' => 'value', '#value' => entity_id('/entity_name/' ,$/entity_name/));
  $/entity_name/_uri = entity_uri('/entity_name/', $/entity_name/);
  return confirm_form($form,
    t('Are you sure you want to delete /entity_name/ %title?', array('%title' => entity_label('/entity_name/', $/entity_name/))),
    $/entity_name/_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function /entity_name/_delete_form_submit($form, &$form_state) {
  $/entity_name/ = $form_state['/entity_name/'];
  /entity_name/_delete($/entity_name/);

  drupal_set_message(t('/entity_name/ %title deleted.', array('%title' => entity_label('/entity_name/', $/entity_name/))));

  $form_state['redirect'] = '<front>';
}
