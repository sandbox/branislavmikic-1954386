<?php

/**
 * Implements hook_entity_info().
 */
function /module_name/_entity_info() {
  $return = array(
    '/entity_name/' => array(
      'label' => t('/entity_label/'),
      'entity class' => '/entity_name/',
      'controller class' => '/entity_controller_name/',
      'base table' => '/entity_schema_name/',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => '/entity_schema_primary_identifier/',
        'bundle' => 'type',
      ),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'bundles' => array(),
      'load hook' => '/entity_name/_load',
      'view modes' => array(
        'full' => array(
          'label' => t('Default'),
          'custom settings' => FALSE,
        ),
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'module' => '/module_name/',
      'access callback' => '/module_name/_access',
    ),
  );

  $return['/entity_name/_type'] = array(
    'label' => t('/entity_name/ Type'),
    'entity class' => '/entity_name/Type',
    'controller class' => 'storageTypeController',
    'base table' => '/entity_schema_name/_type',
    'fieldable' => FALSE,
    'bundle of' => '/entity_name/',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'module' => '/module_name/',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/storage-types',
      'file' => '/module_name/.admin.inc',
      'controller class' => '/entity_name/TypeUIController',
    ),
    'access callback' => '/entity_type/_type_access',
  );

  return $return;
}

/**
 * Implements hook_entity_info_alter().
 */
function ec_storage_entity_info_alter(&$entity_info) {
  foreach (ec_storage_types() as $type => $info) {
    $entity_info['ec_storage']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/storage-types/manage/%ec_storage_type',
        'real path' => 'admin/structure/storage-types/manage/' . $type,
        'bundle argument' => 4,
      ),
    );
  }
}

/**
 * Implements hook_menu().
 */
function ec_storage_menu() {
  $items = array();

  $items['storage/add'] = array(
    'title' => 'Add storage',
    'page callback' => 'ec_storage_admin_add_page',
    'access arguments' => array('administer ec_storage entities'),
    'file' => 'ec_storage.admin.inc',
    'type' => MENU_LOCAL_ACTION,
    'tab_parent' => 'storage',
    'tab_root' => 'storage',
  );

  $storage_uri = 'storage/%ec_storage';
  $storage_uri_argument_position = 1;

  $items[$storage_uri] = array(
    'title callback' => 'entity_label',
    'title arguments' => array('ec_storage', $storage_uri_argument_position),
    'page callback' => 'ec_storage_view',
    'page arguments' => array($storage_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'ec_storage', $storage_uri_argument_position),
    'file' => 'ec_storage.pages.inc',
  );

  $items[$storage_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items[$storage_uri . '/delete'] = array(
    'title' => 'Delete storage',
    'title callback' => 'ec_storage_label',
    'title arguments' => array($storage_uri_argument_position),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ec_storage_delete_form', $storage_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'ec_storage', $storage_uri_argument_position),
    'file' => 'ec_storage.admin.inc',
  );

  $items[$storage_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ec_storage_form', $storage_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'ec_storage', $storage_uri_argument_position),
    'file' => 'ec_storage.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  foreach (ec_storage_types() as $type => $info) {
    $items['storage/add/' . $type] = array(
      'title' => 'Add storage',
      'page callback' => 'ec_storage_add',
      'page arguments' => array(2),
      'access callback' => 'entity_access',
      'access arguments' => array('create', 'ec_storage', $type),
      'file' => 'ec_storage.admin.inc',
    );
  }

  $items['admin/structure/storage-types/%ec_storage_type/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ec_storage_type_form_delete_confirm', 4),
    'access arguments' => array('administer ec_storage types'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'ec_storage.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function ec_storage_permission() {
  $permissions = array(
    'administer ec_storage types' => array(
      'title' => t('Administer storage types'),
      'description' => t('Allows users to configure storage types and their fields.'),
      'restrict access' => TRUE,
    ),
    'create ec_storage entities' => array(
      'title' => t('Create storages'),
      'description' => t('Allows users to create storages.'),
      'restrict access' => TRUE,
    ),
    'view ec_storage entities' => array(
      'title' => t('View storages'),
      'description' => t('Allows users to view storages.'),
      'restrict access' => TRUE,
    ),
    'edit any ec_storage entities' => array(
      'title' => t('Edit any storages'),
      'description' => t('Allows users to edit any storages.'),
      'restrict access' => TRUE,
    ),
    'edit own ec_storage entities' => array(
      'title' => t('Edit own storages'),
      'description' => t('Allows users to edit own storages.'),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}


/**
 * Implements hook_entity_property_info_alter().
 */
function ec_storage_entity_property_info_alter(&$info) {
  $properties = &$info['ec_storage']['properties'];
  $properties['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the node was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer nodes',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'description' => t("The date the node was most recently updated."),
  );
  $properties['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the storage."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer ec_storage entities',
    'required' => TRUE,
    'schema field' => 'uid',
  );
}


/*******************************************************************************
 ********************************* storage API's **********************************
 ******************************************************************************/

/**
 * Access callback for storage.
 */
function ec_storage_access($op, $storage, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }
  switch ($op) {
    case 'create':
      return user_access('administer ec_storage entities', $account)
          || user_access('create ec_storage entities', $account);
    case 'view':
      return user_access('administer ec_storage entities', $account)
          || user_access('view ec_storage entities', $account);
    case 'edit':
      return user_access('administer ec_storage entities')
          || user_access('edit any ec_storage entities')
          || (user_access('edit own ec_storage entities') && ($storage->uid == $account->uid));
  }
}

/**
 * Load a storage.
 */
function ec_storage_load($id_storage, $reset = FALSE) {
  $storages = ec_storage_load_multiple(array($id_storage), array(), $reset);
  return reset($storages);
}

/**
 * Load multiple storages based on certain conditions.
 */
function ec_storage_load_multiple($id_storages = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('ec_storage', $id_storages, $conditions, $reset);
}

/**
 * Save storage.
 */
function ec_storage_save($storage) {
  entity_save('ec_storage', $storage);
}

/**
 * Delete single storage.
 */
function ec_storage_delete($storage) {
  entity_delete('ec_storage', entity_id('ec_storage' ,$storage));
}

/**
 * Delete multiple storages.
 */
function ec_storage_delete_multiple($storage_ids) {
  entity_delete_multiple('ec_storage', $storage_ids);
}


/*******************************************************************************
 ****************************** storage Type API's ********************************
 ******************************************************************************/

/**
 * Access callback for storage Type.
 */
function ec_storage_type_access($op, $entity = NULL) {
  return user_access('administer ec_storage types');
}

/**
 * Load storage Type.
 */
function ec_storage_type_load($storage_type) {
  return ec_storage_types($storage_type);
}

/**
 * List of storage Types.
 */
function ec_storage_types($type_name = NULL) {
  $types = entity_load_multiple_by_name('ec_storage_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Save storage type entity.
 */
function ec_storage_type_save($storage_type) {
  entity_save('ec_storage_type', $storage_type);
}

/**
 * Delete single case type.
 */
function ec_storage_type_delete($storage_type) {
  entity_delete('ec_storage_type', entity_id('ec_storage_type' ,$storage_type));
}

/**
 * Delete multiple case types.
 */
function ec_storage_type_delete_multiple($storage_type_ids) {
  entity_delete_multiple('ec_storage_type', $storage_type_ids);
}

/**
  * Implements hook_views_api().
  */
function ec_storage_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'ec_storage'),
  );
}

