<?php
/**
 * @file
 *  entity structure and module build
 *
 *   to run this command, execute `drush --include=./examples make-entity`
 *   from within your drush directory.
 *
 *   you can copy this file to any of the following
 *     1. a .drush folder in your home folder.
 *     2. anywhere in a folder tree below an active module on your site.
 *     3. /usr/share/drush/commands (configurable)
 *     4. in an arbitrary folder specified with the --include option.
 *     5. drupal's sites/all/drush folder.
 */

/**
 * Implementation of hook_drush_command().
 *
 * @return
 *   An associative array describing your command(s).
 */
function entity_module_builder_drush_command() {
  $items = array();

  // The 'make-entity' command
  $items['make-entity'] = array(
    'description' => "Makes entity structure as module.",
    'arguments' => array(
      'filling' => 'The type of the sandwich (turkey, cheese, etc.). Defaults to ascii.',
    ),
    'options' => array(
      'spreads' => array(
        'description' => 'Comma delimited list of spreads.',
        'example-value' => 'mayonnaise,mustard',
      ),
    ),
    //'aliases' => array(''),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'. This hook is optional. If a command
 * does not implement this hook, the command's description is used instead.
 *
 * This hook is also used to look up help metadata, such as help
 * category title and summary.  See the comments below for a description.
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function entity_module_builder_drush_help($section) {
  switch ($section) {
    case 'drush:make-entity':
      return dt("This command will make you a entity structure and module");
 }
}

/**
 * Implementation of drush_hook_COMMAND_validate().
 *
 * The validate command should exit with
 * `return drush_set_error(...)` to stop execution of
 * the command.  In practice, calling drush_set_error
 * OR returning FALSE is sufficient.  See drush.api.php
 * for more details.
 */
function drush_entity_module_builder_validate() {
  if (drush_is_windows()) {
    // $name = getenv('USERNAME');
    // TODO: implement check for elevated process using w32api
    // as sudo is not available for Windows
    // http://php.net/manual/en/book.w32api.php
    // http://social.msdn.microsoft.com/Forums/en/clr/thread/0957c58c-b30b-4972-a319-015df11b427d
  }
  else {
    $name = posix_getpwuid(posix_geteuid());
    if ($name['name'] !== 'root') {
      return drush_set_error('MAKE_IT_YOUSELF', dt('What? Make your own sandwich.'));
    }
  }
}

/**
 * Example drush command callback. This is where the action takes place.
 *
 * The function name should be same as command name but with dashes turned to
 * underscores and 'drush_commandfile_' prepended, where 'commandfile' is
 * taken from the file 'commandfile.drush.inc', which in this case is 'sandwich'.
 * Note also that a simplification step is also done in instances where
 * the commandfile name is the same as the beginning of the command name,
 * "drush_example_example_foo" is simplified to just "drush_example_foo".
 * To also implement a hook that is called before your command, implement
 * "drush_hook_pre_example_foo".  For a list of all available hooks for a
 * given command, run drush in --debug mode.
 *
 * If for some reason you do not want your hook function to be named
 * after your command, you may define a 'callback' item in your command
 * object that specifies the exact name of the function that should be
 * called.  However, the specified callback function must still begin
 * with "drush_commandfile_" (e.g. 'callback' => "drush_example_foo_execute")
 * if you want that all hook functions are still called (e.g.
 * drush_example_pre_foo_execute, and so on).
 *
 * In this function, all of Drupal's API is (usually) available, including
 * any functions you have added in your own modules/themes.
 *
 * @see drush_invoke()
 * @see drush.api.php
 */
function drush_entity_module_builder_make_entity() {

  //drush_confirm('Are you sure you want to continue?', $indent = 0);

  // give me some inputs about entity and module
  $module_name                =  drush_prompt('Module name');
  $module_description         =  drush_prompt('Module description', $module_name, FALSE); 
  $entity_name                =  drush_prompt('Entity name', $module_name, FALSE);
  $entity_schema_name         =  drush_prompt('Entity schema name', $entity_name, FALSE);
  $entity_primary_identifier  =  drush_prompt('Entity schema primary identifier', 'id_'.$entity_name, FALSE);
  $entity_controller_name     =  drush_prompt('Entity controller name', $entity_name.'Controller', FALSE);
  $default_menu               =  drush_prompt('Default menu', $entity_name, FALSE);

  /*
  // module
  module_name
  module_description

  // entity
  entity_name
  entity_label (name)
  entity_controller_class_name
  entity_class_name

  // shema
  entity_shema_name
  entity_shema_primary_identifier
  entity_shema_custom_field
  */

  // template/pattern files for new module
  $module_templates = array(
    //'_ERROR_TEST_entity_module_builder.info.pattern',
    //'entity_module_builder.install.pattern',
    'module_template_info',
    'module_template_install',
    'module_template_module',
    'module_template_controller.inc',
    'module_template_admin.inc',
    'module_template_pages.inc',
    'module_template_views_default.inc',
  );

  // replacments ( find => replace pattern)
  $code_patterns  = array(
    '/module_name/'                       =>  $module_name,
    '/module_description/'                =>  $module_description,
    '/entity_schema_name/'                =>  $entity_schema_name,
    '/entity_schema_primary_identifier/'  =>  $entity_primary_identifier,
    '/entity_name/'                       =>  $entity_name,
    '/entity_controller_name/'            =>  $entity_controller_name,
  );

  // make module directory
  mkdir($module_name);
  
  // get current path
  $path = getcwd();
  
  // load patterns code
  // find and replace by patterns
  // write to disk
  foreach($module_templates as $module_template_file) {

    // haystack
    $content = @file_get_contents($module_template_file, FILE_USE_INCLUDE_PATH);

    if (!$content) { 
      
      // throw error
      drush_set_error('Error pattern',  dt('Error creating ' . $module_template_file . '\n Pattern not found.'));
    
    } else {
      
      // find, replace patterns
      foreach($code_patterns as $find_pattern => $replace_pattern) {
        $content  = str_replace($find_pattern, $replace_pattern, $content);
      }

      // write to new file
      $module_file = $path . '/' . $module_name . '/'. str_replace('module_template_', $module_name.'.', $module_template_file);
      file_put_contents($module_file, $content);

      // tmp output 
      //drush_print($content);
     
     }
  }

/*
// module
module_name
module_description


// entity
entity_name
entity_label (name)
entity_controller_class_name
entity_class_name


// shema
entity_shema_name
entity_shema_primary_identifier
entity_shema_custom_field

      $entity_shema_custom_field = 
      '/entity_shema_custom_field/' => array(
        'description' => '/entity_shema_custom_field/',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
 */

  // final complete message
  //drush_print("\n" . $module_name . "\n");
}

/**
 * Command argument complete callback. Provides argument
 * values for shell completion.
 *
 * @return
 *  Array of popular fillings.
 */
function entity_module_builder_complete() {
  return array('values' => array('turkey', 'cheese', 'jelly', 'butter'));
}
