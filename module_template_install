<?php

/**
 * Implements hook_schema().
 */
function /module_name/_schema() {
  $schema = array();

  $schema['/entity_schema_name/'] = array(
    'description' => 'The base table for /entity_name/.',
    'fields' => array(
      '/entity_schema_primary_identifier/' => array(
        'description' => 'The primary identifier for the /entity_name/',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type (bundle) of /entity_name/ (/entity_name/ type).',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
     'uid' => array(
        'description' => 'ID of Drupal user creator.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'TheqUnix timestamp when the /entity_name/ was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the /entity_name/ was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'description' => array(
        'description' => '/entity_name/ description.',
        'type' => 'text',
      ),
      //  
      // /entity_schema_custom_field/
      //
    ),
    'primary key' => array('/entity_schema_primary_identifier/'),
  );

  $schema['/entity_schema_name/_type'] = array(
    'description' => 'Stores information about all defined storage types (/entity_name/ types).',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique storage type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array('type' => array('type'),),
  );

  return $schema;
}

/**
 * Implements _hook_install()
 */
function /module_name/_install() {

  $entity_type = '/module_name/';

  $fields = _/module_name/_fields();

  // crete fields
  foreach($fields as $field) {
    if (!field_info_field($field['field_name'])) {
      field_create_field($field);
    }
  }

  // included bundles
  $bundles = array(
    'standard' => (object) array(
      'type' => 'standard',
      'label' => 'Standard /module_name/ bundle',
      'description' => '/module_name/ description',
      'is_new' => TRUE,
      'uid' => 1,
    ),
  );

  // create bundles and instaces
  foreach($bundles as $bundle) {
    $controller = new /module_name/TypeController('/module_name/_type');
    $controller->save($bundle); 
    // also attach included instaces
    // @REFERENCE @/module_name/.module:function /module_name/_field_attach_create_bundle() 
  }

}

/**
 * Implements _hook_uninstall()
 */
function /module_name/_uninstall() {
  $fields = _/module_name/_fields();
  foreach($fields as $field) {
    if (field_info_field($field['field_name'])) {
      field_delete_field($field['field_name']);
    }
  }
}

/**
 * _/module_name/_fields()
 */
function _/module_name/_fields() {
  /*
   * demo field 
   *
	  $fields = array(
	     REFERENCE
	    '/module_name/_fieldname_reference' => array(
	      'field_name' => '/module_name/_fieldname_reference',
	      'type' => 'entityreference',
	      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
	      'settings' => array(
		'target_type' => '/module_name/',
		'handler' => 'views',
		'handler_settings' => array(
		  'view' => Array (
		    'view_name' => '/module_name/_viewname_reference',
		    'display_name' => 'entityreference_1',
		    'args' => Array (),
		  ),
		  'target_bundles' => array(),
		),
	      ),
	    ), SELECT LIST
	    '/module_name/_fieldname' => array(
	      'field_name' => '/module_name/_fieldname',
	      'label' => t('Select fieldname label text'),
	      'type' => 'list_text',
	      'cardinality' => '1',
	      'module' => 'list',
	      'settings' => array(
		'allowed_values' => array(
		  'testvalue' => 'testvalue',
		  'testvalue2' => 'testvalue2',
		),
	      ),
	    ), TEXT
	    '/module_name/_fieldname' => array(
	      'field_name' => '/module_name/_fieldname',
	      'type' => 'text',
	    ),
	  );
  */

  // default for drush make ->no fields
  $fields = array();

  return $fields;
}
